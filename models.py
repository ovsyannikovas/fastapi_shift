from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from . import database


class User(database.Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    login = Column(String, unique=True, index=True)
    hashed_password = Column(String)

    salary = relationship("Salary", back_populates="user")
    token = relationship("Token", back_populates="user")


class Salary(database.Base):
    __tablename__ = "salaries"

    id = Column(Integer, primary_key=True, index=True)
    salary = Column(Integer)
    promotion_date = Column(String)
    user_id = Column(Integer, ForeignKey("users.id"))

    user = relationship("User", back_populates="salary")


class Token(database.Base):
    __tablename__ = "tokens"

    id = Column(Integer, primary_key=True, index=True)
    token = Column(String)
    token_timestamp = Column(String, default=None)
    user_id = Column(Integer, ForeignKey("users.id"))

    user = relationship("User", back_populates="token")
