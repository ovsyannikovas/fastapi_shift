from datetime import datetime

from sqlalchemy.orm import Session
import uuid
import hashlib

from . import models, schemas


def get_user_by_login(db: Session, login: str):
    return db.query(models.User).filter(models.User.login == login).first()


def get_token_item_by_token(db: Session, token: str):
    return db.query(models.Token).filter(models.Token.token == token).first()


def get_token_item_by_user_id(db: Session, user_id: int):
    return db.query(models.Token).filter(models.Token.user_id == user_id).first()


def get_salary_item(db: Session, user_id: int):
    return db.query(models.Salary).filter(models.Salary.user_id == user_id).first()


def generate_user_token(db: Session, user_id: int):
    token_item = get_token_item_by_user_id(db, user_id)
    if token_item:
        db_item = token_item
        db_item.token = str(uuid.uuid4())
        db_item.token_timestamp = datetime.utcnow()
    else:
        db_item = models.Token(token=str(uuid.uuid4()), token_timestamp=datetime.utcnow(), user_id=user_id)
        db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item


def edit_token_timestamp(db: Session, user_id: int, new_timestamp: str):
    token_item = get_token_item_by_user_id(db, user_id)
    token_item.token_timestamp = new_timestamp
    db.commit()
    db.refresh(token_item)
    return token_item


def create_user(db: Session, user: schemas.User):
    existent_user = get_user_by_login(db, user.login)
    if existent_user:
        return existent_user
    hashed_password = hashlib.sha256(user.password.encode()).hexdigest()
    db_user = models.User(login=user.login, hashed_password=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def create_salary(db: Session, user):
    existent_salary = get_token_item_by_user_id(db, user.user_id)
    if existent_salary:
        return existent_salary
    db_salary = models.Salary(salary=user.salary, user_id=user.user_id, promotion_date=user.promotion_date)
    db.add(db_salary)
    db.commit()
    db.refresh(db_salary)
    return db_salary


def delete_user(db: Session, login: str):
    db_user = get_user_by_login(db, login)
    if db_user:
        db.delete(db_user)
        db.commit()
    return


def delete_salary(db: Session, user_id: int):
    db_salary = get_salary_item(db, user_id)
    if db_salary:
        db.delete(db_salary)
        db.commit()
    return


def delete_token(db: Session, user_id: int):
    db_token = get_token_item_by_user_id(db, user_id)
    if db_token:
        db.delete(db_token)
        db.commit()
    return
