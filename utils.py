import hashlib
import datetime
import time


def hash_pass(password: str):
    return hashlib.sha256(password.encode()).hexdigest()


def get_time(time_string: str):
    return time.mktime(datetime.datetime.strptime(time_string, "%Y-%m-%d %H:%M:%S.%f").timetuple())
