from datetime import datetime

from fastapi import Depends, FastAPI
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

from . import crud, models, schemas, database, utils

models.database.Base.metadata.create_all(bind=database.engine)

app = FastAPI()


# Dependency
def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post('/get_token')
def get_token(request: schemas.User, db: Session = Depends(get_db)):
    user = crud.get_user_by_login(db, request.login)
    if user and utils.hash_pass(request.password) == user.hashed_password:
        return crud.generate_user_token(db, user.id)
    return JSONResponse(content={"error": "Неверный логин или пароль."}, status_code=403)


@app.post('/show_salary')
def show_salary(token: str, db: Session = Depends(get_db)):
    token = crud.get_token_item_by_token(db, token)
    delta = 600.0  # 10 minutes
    if token:
        token_delta = utils.get_time(str(datetime.utcnow())) - utils.get_time(token.token_timestamp)
        if token_delta < delta:
            return crud.get_salary_item(db, token.user_id)
        return JSONResponse(content={"error": "Токен недействителен."}, status_code=403)
    return JSONResponse(content={"error": "Неверный токен."}, status_code=403)

# @app.post('/create_user')
# def create_user(request: schemas.User, db: Session = Depends(get_db)):
#     user = crud.create_user(db, request)
#     return user
#
#
# @app.post('/create_salary')
# def create_salary(request: schemas.Salary, db: Session = Depends(get_db)):
#     salary = crud.create_salary(db, request)
#     return salary
