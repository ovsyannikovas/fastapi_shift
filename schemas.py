from pydantic import BaseModel


class User(BaseModel):
    login: str
    password: str


class Salary(BaseModel):
    salary: int
    promotion_date: str
    user_id: int


class Token(BaseModel):
    id: int
    token_timestamp: str
    token: str
    user_id: int
