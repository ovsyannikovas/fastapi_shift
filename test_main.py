import datetime

import pytest
from fastapi.testclient import TestClient

from . import crud
from faker import Faker
from . import database, schemas, utils
from .main import app

client = TestClient(app)


@pytest.fixture(scope='module')
def db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@pytest.fixture(scope='module', autouse=True)
def prepare(db):
    fake: Faker = Faker()
    login, password = fake.user_name(), fake.user_name()
    salary = fake.random.randint(1, 99999)
    promotion_date = '.'.join(map(str, (
        fake.random.randint(1, 30),
        fake.random.randint(1, 12),
        fake.random.randint(2000, 3000)
    )))

    user_data = schemas.User(**{'login': login, 'password': password})
    user = crud.create_user(db, user=user_data)
    salary_data = schemas.Salary(**{'salary': salary, 'promotion_date': promotion_date, 'user_id': user.id})
    crud.create_salary(db, user=salary_data)
    yield {'login': login, 'password': password, 'promotion_date': promotion_date, 'salary': salary, 'user_id': user.id}
    crud.delete_salary(db, user.id)
    crud.delete_token(db, user.id)
    crud.delete_user(db, login)


class TestGettingToken:

    def test_get_token_with_valid_data(self, prepare: dict):
        login, password = prepare['login'], prepare['password']
        response = client.post("/get_token", json={"login": login, "password": password})
        response_json = response.json()
        assert response.status_code == 200
        assert response_json['user_id'] == prepare['user_id']
        assert isinstance(response_json['token_timestamp'], str)
        assert isinstance(response_json['token'], str)

    def test_get_token_with_invalid_login(self, prepare: dict):
        fake: Faker = Faker()
        login, password = fake.user_name(), prepare['password']
        response = client.post("/get_token", json={"login": login, "password": password})
        assert response.status_code == 403
        assert response.json() == {'error': 'Неверный логин или пароль.'}

    def test_get_token_with_invalid_password(self, prepare: dict):
        fake: Faker = Faker()
        login, password = prepare['login'], fake.user_name()
        response = client.post("/get_token", json={"login": login, "password": password})
        assert response.status_code == 403
        assert response.json() == {'error': 'Неверный логин или пароль.'}

    def test_get_token_with_valid_data_two_times(self, prepare: dict):
        login, password = prepare['login'], prepare['password']
        response = client.post("/get_token", json={"login": login, "password": password})
        response_json = response.json()
        first_token, first_timestamp = response_json['token'], response_json['token_timestamp']
        assert response.status_code == 200
        assert response_json['user_id'] == prepare['user_id']
        assert isinstance(response_json['token_timestamp'], str)
        assert isinstance(response_json['token'], str)

        response = client.post("/get_token", json={"login": login, "password": password})
        response_json = response.json()
        assert response.status_code == 200
        assert response_json['user_id'] == prepare['user_id']
        assert isinstance(response_json['token_timestamp'], str)
        assert isinstance(response_json['token'], str)
        assert response_json['token'] != first_token
        assert response_json['token_timestamp'] != first_timestamp


class TestShowingSalary:
    def test_show_salary_with_valid_token(self, prepare: dict):
        login, password = prepare['login'], prepare['password']
        response = client.post("/get_token", json={"login": login, "password": password})
        token = response.json()['token']
        response = client.post('/show_salary', params={'token': token})
        assert response.status_code == 200
        assert response.json()['salary'] == prepare['salary']
        assert response.json()['promotion_date'] == prepare['promotion_date']
        assert response.json()['user_id'] == prepare['user_id']

    def test_show_salary_with_invalid_token(self, prepare: dict):
        login, password = prepare['login'], prepare['password']
        response = client.post("/get_token", json={"login": login, "password": password})
        token = response.json()['token'] + 'hello'
        response = client.post('/show_salary', params={'token': token})
        assert response.status_code == 403
        assert response.json() == {'error': 'Неверный токен.'}

    def test_show_salary_with_expired_token(self, prepare: dict, db):
        login, password = prepare['login'], prepare['password']
        response = client.post("/get_token", json={"login": login, "password": password})
        token = response.json()['token']
        new_timestamp = datetime.datetime.fromtimestamp(utils.get_time(response.json()['token_timestamp']) - 700.0)
        crud.edit_token_timestamp(db, new_timestamp=str(new_timestamp) + '.0', user_id=response.json()['user_id'])
        response = client.post('/show_salary', params={'token': token})
        assert response.status_code == 403
        assert response.json() == {'error': 'Токен недействителен.'}
